﻿using Newtonsoft.Json;
using RabbitMQ.Client;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;

namespace RabbitMQConsole
{
    public static class TopicExchangeProducer
    {
        public static void Producer(IModel channel)
        {
            var ttl = new Dictionary<string, object>
            {
                { "x-message-ttl", 30000 }
            };

            //Time to Leave ttl
            channel.ExchangeDeclare("demo-topic-exchange", ExchangeType.Topic, arguments: ttl);

            var count = 0;
            Console.WriteLine("Producer started");

            while (true)
            {
                var message = new { Name = "Producer", Message = $"Hello!!! count: {count}", UserId = Guid.NewGuid() };

                var body = Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(message));

                channel.BasicPublish("demo-topic-exchange", "account.init", null, body);

                count++;

                Thread.Sleep(1000);
            }
        }
    }
}
