﻿using Newtonsoft.Json;
using RabbitMQ.Client;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;

namespace RabbitMQConsole
{
    public static class FanoutExchangeProducer
    {
        public static void Producer(IModel channel)
        {
            var ttl = new Dictionary<string, object>
            {
                { "x-message-ttl", 30000 }
            };

            //Time to Leave ttl
            channel.ExchangeDeclare("demo-fanout-exchange", ExchangeType.Fanout, arguments: ttl);

            var count = 0;
            Console.WriteLine("Producer started");

            while (true)
            {
                var message = new { Name = "Producer", Message = $"Hello!!! count: {count}", UserId = Guid.NewGuid() };

                var body = Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(message));

                var property = channel.CreateBasicProperties();

                property.Headers = new Dictionary<string, object> { { "account", "new" } };

                channel.BasicPublish("demo-fanout-exchange", string.Empty, property, body);

                count++;

                Thread.Sleep(1000);
            }
        }
    }
}
