﻿using RabbitMQ.Client;
using System;

namespace RabbitMQConsole
{
    static class Program
    {
        static void Main(string[] args)
        {
            var factory = new ConnectionFactory
            {
                Uri = new Uri("amqp://guest:guest@localhost:5672")
            };

            using var connection = factory.CreateConnection();
            using var channel = connection.CreateModel();

            //ProducerQueue.Publish(channel);
            //DirectExchangeProducer.Publish(channel);
            //TopicExchangeProducer.Producer(channel);
            //HeaderExchangeProducer.Producer(channel);

            FanoutExchangeProducer.Producer(channel);
        }
    }
}
